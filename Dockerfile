FROM microsoft/dotnet:2-sdk

COPY ValueApi/bin/Release/netcoreapp2.2 /app

ENTRYPOINT ["dotnet", "/app/ValueApi.dll"]

